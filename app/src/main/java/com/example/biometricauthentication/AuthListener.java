package com.example.biometricauthentication;

public interface AuthListener {

    void getResultType(FingerprintHandler.ResultType result);
}
